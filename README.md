# HotelService

Zadania bloku Programowanie I -
javaszc9
Napisz program pozwalający na rezerwację miejsc hotelowych. Projekt swój wykonuj
etapami a wyniki zapisuj w repozytorium zdalnym. Postępuj według poniższych wskazówek:
- Projekt powinien być dobrze opisany. Metody powinny posiadać komentarze, wiadomości
commitów powinny zawierać nazwy dodanych w nich funkcjonalności.
- Struktura projektu jest narzucona w treści zadania, nie jest obowiązkowa, jednak trzymanie
się jej gwarantuje łatwość w utrzymaniu porządku
- W treści zdań pojawiają się odnośniki do tutoriali/porad dotyczących wykonania danego
podpunktu, uczą one korzystania ze źródeł i wdrażania nieznanych wcześniej
funkcjonalności. Jeśli jakaś podpowiedź sprawia Ci trudność poszukaj alternatywy, w razie
dalszych problemów – pytaj!
- Jakością dobrze zrobionego zadania będzie również trzymanie się git-flow, tj. tworzenie
osobnych gałęzi dla kolejnych funkcjonalności

Część I

1. Utwórz nowe repozytorium o nazwie HotelService a następnie wypchnij na nie nowo
utworzony (pusty!) projekt. Pamiętaj o zignorowaniu niepotrzebnych plików
2. Utwórz gałąź develop i na niej kontynuuj prace
3. Wszystkie nowe funkcjonalności wprowadzaj na nowych gałęziach a następnie
dołączaj je do gałęzi develop np. branch LIST_OF_ROOMS, dla nowej funkcjonalności
wyświetlania wszystkich pokoi
4. Dodaj klasę Room reprezentującą pojedynczy pokój (nr pokoju, ilu osobowy, czy w
pokoju jest łazienka - true/false oraz czy pokój jest dostępny - true/false).
5. Dodaj klasę Hotel która będzie zawierała listę pokoi.
6. W konstruktorze klasy Hotel utwórz kilka obiektów klasy Room i dodaj je do listy (np.
10-15), dzięki temu podczas tworzenia instancji obiektu hotel, automatycznie
generowana będzie lista pokoi.
7. Dodaj klasę HotelService która będzie służyć do obsługi hotelu. Na początek dodaj
funkcjonalności:
1. Pobierz listę wszystkich pokoi.
2. Pobierz listę wszystkich dostępnych pokoi.
3. Rezerwuj pokój (podaj nr pokoju i jeśli jest dostępny to go zarezerwuj).

Szczecin 15 kwiecien 2019

4. Zwolnij pokój (podaj nr pokoju i jeśli jest zajęty to go zwolnij).
8. W klasie main utwórz proste menu do obsługi hotelu, przykłady implementacji
(prosta pętla do…while + switch):
1. https://stackoverﬂow.com/a/13536215/5877109
2. http://chronicles.blog.ryanrampersad.com/2011/03/text-based-menu-in-java/
9. Do menu dodaj funkcję:
1. Wyświetl listę pokoi wraz z ich statusem (wolny-zajęty) korzystając z HotelService.
2. Wyświetl listę tylko dostępnych pokoi.
3. Zarezerwuj pokój.
4. Zwolnij pokój.
10. Zapisz i zrób commit na nowej gałęzi a następnie wykonaj merge swojej gałęzi z
developem i usuń gałąź. Jeśli chcesz abym zrobił dla Ciebie code review to pamiętaj,
nie merguj do gałęzi master, niech pozostanie pusta.
Część II
Dla kolejnych funkcjonalności twórz nowe gałęzie.
1. Dla pkt 1, 2 i 3 utwórz jedną nową gałąź. Dodaj klasę Guest która będzie
reprezentowała hotelowego gościa (imię, nazwisko, data urodzenia).
2. Do klasy Room dodaj listę która będzie przechowywała gości zameldowanych w
danym pokoju.
3. Metodę rezerwacji zmodyﬁkuj tak aby móc przekazać nr pokoju oraz gości którzy się
w nim meldują. Zanim zameldujesz gości sprawdź czy przynajmniej 1 osoba jest
pełnoletnia oraz czy liczba gości nadaje się do tego pokoju. Jeśli wszystko działa bez
problemów, wykonaj merge swojej gałęzi do developa.
https://stackoverflow.com/questions/37339357/java-localdate-check-if-18-years-old-
and-above
4. Dla pkt 4, 5 i 6 utwórz jedną nową gałąź. Do klasy Room dodaj parametr pozwalający
zweryﬁkować (true/false) czy pokój jest posprzątany. Domyślnie wszystkie pokoje
będą posprzątane, natomiast jeśli gość się wymelduje, pokój zmienia status na
nieposprzątany. Nieposprzątanego pokoju nie można zarezerwować pomimo że jest
wolny.
5. W klasie HotelService dodaj metodę pozwalającą posprzątać pokój (zmienić status
pola posprzatany na true). Metoda powinna przyjmować jako parametr nr pokoju.
6. W menu dodaj funkcję pozwalającą wyświetlić listę pokoi do posprzątania oraz
funkcję posprzątania pokoju. Wykonaj merge do develop.
7. Dla pkt 7 i 8 utwórz jedną nową gałąź. W klasie Room dodaj pole data zameldowania
oraz data wymeldowania.
8. W menu aplikacji dodaj funkcję pozwalającą wyświetlić listę zajętych pokoi wraz z
datą opuszczenia go. Wykonaj merge wszystkich galezi do developa jeśli wszystkie
funkcjonalności działają.

Część III

Szczecin 15 kwiecien 2019

1.Do klasy pokój utwórz enumerator który będzie deﬁniował standard pokoju (Standard,
Rodzinny oraz Apartament).
2. Do klasy Room dodaj pole w którym przechowywać będziesz cenę pokoju (cena ta
powinna być wyrażona w formacie np. BigDecimal) i wyliczana będzie na podstawie kilku
parametrów.
3. Cena pokoju będzie wyliczana automatycznie (metoda do wyliczania będzie wywoływana
w konstruktorze klasy Room) między innymi na podstawie standardu pokoju oraz piętra na
którym się znajduje (zakładamy że czym wyżej tym goście mają lepszy widok). Aby sprawdzić
na którym piętrze jest pokój, wystarczy sprawdzić nr pokoju (pokoje na parterze będą
numerowane od 1-10, na pierwszym piętrze od 20-30, drugie piętro to pokoje 30 - 40 itd itd
). Zakładamy że hotel ma maksymalnie 10 pięter.
4. Podstawową cenę za pokój zapisz w pliku conﬁg.properties w formacie klucz=wartość
(defaultprice = 100) i utwórz klasę narzędziową (np. ConﬁgFileUtil) w której
zaimplementujesz metodę do wczytywania danych z pliku (krótki opis jak to zrobić:
https://www.mkyong.com/java/java-properties-file-examples/) Dzięki temu że właściwość ta
nie będzie na stałe zapisana w klasie Javy, będziesz mógł w każdej chwili zmieniać tę cenę.
5. Podstawowa cena pokoju zdeﬁniowana w pliku conﬁg.properties to cena za pokój typu
Standard na parterze. Założenia do pozostałych cen pokojów:
 Cena pokoju rodzinnego to powiększona o 1/2 cena pokoju standard (Jeśli
podstawowa cena za pokój wynosi 100 zł to cena za pokój rodzinny na parterze
wynosi 150zł).
 Cena pokoju typu Apartament to powiększona dwukrotnie cena pokoju standard.
 Za widok z każdego wyższego piętra klient musi zapłacić dodatkowo 5% kwoty
podstawowej.
 Podsumowując, pokój na 10 piętrze typu apartament przy cenie podstawowej 100zł
to koszt 250zł za dobę.
6. Dodaj możliwość obniżenia ceny za pokój dzięki użyciu kodu rabatowego (HotelService). W
tym celu zdeﬁniuj kod w pliku conﬁg.properties (do jego wczytywania wykorzystaj wcześniej
utworzoną metodę w klasie ConﬁgFileUtil).
7.String z kodem rabatowym powinien być przyjmowany przez metodę odpowiedzialna za
bookowanie pokoju, zabezpiecz się przed brakiem kodu tj. nullem. Zrób również tak aby przy
wymeldowywaniu cena pokoju była kasowana.
8. Do klasy pokój dodaj enumerator pozwalający zdeﬁniować czy zameldowane osoby będą
korzystać z wyżywienia. Enumerator powinien posiadać kilka wartości:
 OV (overnight) – sam nocleg, bez wyżywienia
 BB (bed &amp; breakfast) – samo śniadanie
 HB (half board) – śniadanie i obiadokolacja
 FB (full board) – pełne wyżywienie

Szczecin 15 kwiecien 2019

 AI (all inclusive) – pełne wyżywienie oraz napoje bezalkoholowe i alkohole
9. Ceny za poszczególne pakiety wyżywienia zdeﬁniuj w pliku properties.
9.Przerób swoją aplikację, tak aby liczenie ceny było trochę bardziej generyczne. Utwórz
nową klasę o nazwie np. BookingProperties. Klasa ta będzie grupowała wszystkie właściwości
rezerwacji. Powinny znaleźć się w niej pola takie jak nr pokoju, rodzaj wyżywienia, kod
rabatowy oraz lista gości.
10.W klasie HotelService popraw swoją metodę do bukowania pokoju w taki sposób, aby
przyjmowała jako parametr obiekt klasy BookingProperties. Na tej podstawie od teraz będzie
wyliczana cena za pokój którą goście będą musieli uiścić przy wymeldowaniu się z hotelu.

Część IV

1. Dodaj Maven’a do projektu. Można to zrobić poprzez kliknięcie prawym przyciskiem
myszy w nazwę projektu w lewym górnym rogu, wybranie Add Framework Support ,
wybranie Maven z listy -&gt; OK.
2. Utwórz klasę JSONUtil, która będzie pozwalała na zapis/odczyt w osobnych metodach
obiektu Hotel do pliku JSON, opieraj się na przykładach z prezentacji oraz
wykonanych na zajęciach ćwiczeniach. Pamiętaj o tym aby metoda odpowiednio
informowała o wyniku operacji. JSONObject musi zostać zaimportowany przez
mavena. Wystarczy dodać zależność z poniższego linku do pom.xml pomiędzy
znacznikami &lt;dependencies&gt;.
https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple/1.1
Tutorial wykorzystujący ww. bibliotekę:
https://howtodoinjava.com/json/json-simple-read-write-json-examples/
* Quest poboczny Dobrą praktyką by było utworzenie „historii” czyli każdy kolejny
zapis do pliku powinien dopisywać kolejny hotel, oddzielone np. datą
3. Utwórz klasę HotelDatabase, klasa ta będzie odpowiedzialna za
zapisywanie/odczytywanie listy naszych pokoi do/z pliku JSON, zatem utwórz w niej
metody getRoomsFromDatabase and saveRoomsToDatabase i w nich odwołuj się do
klasy JSONUtil
4. Utwórz metodę, która będzie zapisywała listę pokoi do pliku JSON zaraz po
wygenerowaniu pokoi w konstruktorze
5. Utwórz metodę która będzie aktualizowała bazę danych na żądanie użytkownika
(wyczyści plik z poprzednimi danymi oraz zapisze z nowymi)
6. Dla klasy Room dodaj możliwość porównywania pokoi po jego numerze tj.
Comparable, zmodyfikuj metodę wyświetlania pokoi tak żeby wyświetlała je
posortowane wg numerów.
7. Dla Klasy Hotel dodaj COMPARATOR który będzie porównywał dwa hotele po ilości
pokoi.